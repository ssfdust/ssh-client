//! ## Client Module
//!
//! This module contains the client implementation for the SSH protocol,
//! it is a simple client that can connect to a server and authenticate
//! using a password. It is also able to clean up the outdated kwnown_hosts
//! file.
use spdlog::prelude::*;
use thrussh::*;
use thrussh_keys::key;
use std::sync::Arc;
/// The SshClient struct This struct represents the client for the SSH protocol, it is able to
/// connect to a server and authenticate using a password. It is also able
/// to clean up the outdated kwnown_hosts file.
#[derive(Debug, Default, Clone)]
pub struct SshClient {
    server: String,
    port: u32,
    known_hosts: String,
    client_config: Arc<thrussh::client::Config>
}

impl SshClient {
    pub fn new(server: &str, port: u32, kwnown_hosts: &str) -> Self {
        SshClient {
            server: server.to_string(),
            port,
            known_hosts: kwnown_hosts.to_string(),
            client_config: Arc::new(thrussh::client::Config::default())
        }
    }

    /// Set the server address
    pub fn set_server(&mut self, server: &str) {
        self.server = server.to_string();
    }

    /// Set the server port
    pub fn set_port(&mut self, port: u32) {
        self.port = port;
    }

    /// Set the known_hosts file
    pub fn set_known_hosts(&mut self, known_hosts: &str) {
        self.known_hosts = known_hosts.to_string();
    }

    /// Connect to the target server
    pub async fn connect<'a, 'b:'a>(&'b self, username: &'a str, password: &'a str) -> Result<(String, String), thrussh::Error> {
        info!("Connecting to server: {}:{} with username: {}", self.server, self.port, username);
        let connect_string = format!("{}:{}", self.server, self.port);
        let config = Arc::clone(&self.client_config);
        /* How to avoid cloning the client? */
        let client = self.clone();
        let mut session = thrussh::client::connect(config, &connect_string, client).await?;
        if session.authenticate_password(username, password).await.is_err() {
            return Err(thrussh::Error::NotAuthenticated);
        }
        Ok((username.to_string(), password.to_string()))
    }

    /// Clean the outdated hosts from the known_hosts file
    pub async fn check_known_hosts(&self) -> tokio::io::Result<()> {
        info!("Checking known hosts");
        crate::utils::check_known_hosts(&self.server, self.port as u16, &self.known_hosts)?;
        Ok(())
    }
}

impl client::Handler for SshClient {
   type Error = thrussh::Error;
   type FutureUnit = futures::future::Ready<Result<(Self, client::Session), thrussh::Error>>;
   type FutureBool = futures::future::Ready<Result<(Self, bool), thrussh::Error>>;

   fn finished_bool(self, b: bool) -> Self::FutureBool {
       futures::future::ready(Ok((self, b)))
   }

   fn finished(self, session: client::Session) -> Self::FutureUnit {
       futures::future::ready(Ok((self, session)))
   }

   fn check_server_key(self, _server_public_key: &key::PublicKey) -> Self::FutureBool {
       self.finished_bool(true)
   }

   fn channel_open_confirmation(self, channel: ChannelId, _max_packet_size: u32, _window_size: u32, session: client::Session) -> Self::FutureUnit {
       println!("channel_open_confirmation: {:?}", channel);
       self.finished(session)
   }

   fn data(self, channel: ChannelId, data: &[u8], session: client::Session) -> Self::FutureUnit {
       println!("data on channel {:?}: {:?}", channel, std::str::from_utf8(data));
       self.finished(session)
   }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn test_ssh_client() {
        let client = SshClient::new("localhost", 22, "/home/vagrant/.ssh/known_hosts");
        let ret = client.connect("vagrant", "vagrant").await;
        assert!(ret.is_ok());
    }
}
