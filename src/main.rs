mod sshclient;
mod utils;
use clap::Parser;
use futures::stream::FuturesUnordered;
use futures::StreamExt;
use spdlog::prelude::*;
use tokio::io::AsyncBufReadExt;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = "A small ssh burst client")]
struct Arguments {
    server: String,

    #[arg(short, long, default_value_t = 22, help = "the port of the ssh server")]
    port: u32,

    #[arg(short, long, default_value_t = String::from("~/.ssh/ssh-users"), help = "the file containing the users' names")]
    users_file: String,

    #[arg(short, long, default_value_t = String::from("~/.ssh/ssh-passwords"), help = "the file containing the passwords")]
    file: String,
}

#[tokio::main]
async fn main() {
    let args = Arguments::parse();
    let server = args.server.leak();
    let port = Box::leak(Box::new(args.port));

    let passwd_file = tokio::fs::File::open(&utils::expand_path(&args.file)).await.unwrap();
    let users = tokio::fs::read_to_string(&utils::expand_path(&args.users_file))
        .await
        .map(|x| {
            x.split("\n")
                .map(|x| x.to_string())
                .collect::<Vec<String>>()
        })
        .unwrap_or(vec!["root".into()]).leak();
    let passwd_reader = tokio::io::BufReader::new(passwd_file);
    let mut passwd_lines = tokio::io::BufReader::lines(passwd_reader);
    let mut client_instances = FuturesUnordered::new();
    let known_hosts_path = utils::get_default_known_hosts().map(|x| x.leak()).unwrap();

    // Try to connect to the server and check the known hosts file
    debug!("Connecting to the server");
    let mut client = sshclient::SshClient::default();

    client.set_server(server);
    client.set_port(*port);
    client.set_known_hosts(&known_hosts_path);
    client.check_known_hosts().await.unwrap();

    let mut target_user = String::new();
    let mut target_password = String::new();

    while let Ok(Some(password)) = passwd_lines.next_line().await {
        let mut found = false;
        let password = password.leak();
        for username in users.iter() {
            client_instances.push(tokio::task::spawn(async {
                let client = sshclient::SshClient::new(server, *port, known_hosts_path);
                if let Ok((ret_u, ret_p)) = client.connect(username, &*password).await {
                    Ok((ret_u, ret_p))
                } else {
                    Err(())
                }
            }));
        }
        if client_instances.len() >= 100 {
            while let Some(Ok(Ok(ret))) = client_instances.next().await {
                ((target_user, target_password)) = ret;
                found = true;
            }
            if found == true {
                break
            }
        }
    }

    while let Some(Ok(Ok(ret))) = client_instances.next().await {
        (target_user, target_password) = ret;
    }

    if target_user.is_empty() {
        error!("Failed to connect to the server");
        return;
    }
    println!("Connected to the server with username: {} and password: {}", target_user, target_password);
}
