//! Utils Module
//!
//! This module contains the utilities for the SSH protocol, it is a simple
//! module that contains the common functions and structs used by the client.

use ssh2::Error;
use ssh2::KnownHostFileKind;
use ssh2::Session;
use ssh2::{self, CheckResult};
use std::net::TcpStream;
use std::path::Path;

/// The KnownHostsAction enum represents the possible actions that can be
/// performed on the known hosts file.
pub enum KnownHostsAction {
    Added,
    Modified,
    Passed,
}

/// Get the defaut known hosts file
/// The default known hosts file is located in the user home directory
/// under the .ssh directory.
pub fn get_default_known_hosts() -> Option<String> {
    let user_dirs = directories::UserDirs::new()?;
    let known_hosts = user_dirs.home_dir().join(".ssh/known_hosts");
    Some(known_hosts.to_string_lossy().to_string())
}

/// Get the SSH session for the server and port
fn get_ssh_session(server: &str, port: u16) -> Result<Session, Error> {
    let tcp = TcpStream::connect((server, port)).map_err(|_e| {
        ssh2::Error::new(
            ssh2::ErrorCode::Session(101),
            format!("failed to connect to the server {} {}", server, port).leak(),
        )
    })?;
    let mut session = Session::new()?;
    session.set_tcp_stream(tcp);
    session.handshake()?;
    Ok(session)
}

/// Check the known hosts file for the server and port, if the server is not
/// found in the known hosts file, it will add it.
/// if the server is found and the key is different, it will add the new key
/// to the known hosts file.
pub fn check_known_hosts(
    server: &str,
    port: u16,
    host_file: &str,
) -> Result<KnownHostsAction, Error> {
    let session = get_ssh_session(server, port)?;
    let host_file_path = Path::new(host_file);
    let mut known_hosts = session.known_hosts()?;
    let target_host = if port == 22 {
        server.to_string()
    } else {
        format!("[{}]:{}", server, port)
    };

    known_hosts.read_file(&host_file_path, KnownHostFileKind::OpenSSH)?;
    if let Some((key, key_type)) = session.host_key() {
        match known_hosts.check_port(&server, port, key) {
            CheckResult::Match => {
                return Ok(KnownHostsAction::Passed);
            }
            CheckResult::NotFound => {
                known_hosts.add(&target_host, key, server, key_type.into())?;
                known_hosts.write_file(&host_file_path, KnownHostFileKind::OpenSSH)?;
                return Ok(KnownHostsAction::Added);
            }
            CheckResult::Mismatch => {
                known_hosts
                    .hosts()
                    .map(|hosts| {
                        hosts
                            .iter()
                            .map(|host| {
                                let hostname = host.name().unwrap_or("");
                                if hostname == target_host {
                                    known_hosts.remove(&host).unwrap();
                                }
                            })
                            .collect::<Vec<_>>()
                    })
                    .unwrap();
                known_hosts.add(&target_host, key, server, key_type.into())?;
                known_hosts.write_file(&host_file_path, KnownHostFileKind::OpenSSH)?;
                return Ok(KnownHostsAction::Modified);
            }
            CheckResult::Failure => panic!("failed to check the known hosts"),
        }
    } else {
        Err(ssh2::Error::new(
            ssh2::ErrorCode::Session(100),
            "cannot get the host key",
        ))
    }
}

/// Expand user directory
pub fn expand_path(path: &str) -> String {
    if path.starts_with("~") {
        let userdir = directories::UserDirs::new().unwrap();
        let home = userdir.home_dir();
        home.join(&path[2..]).to_string_lossy().to_string()
    } else {
        path.to_string()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use directories::UserDirs;

    #[test]
    fn test_known_hosts() -> Result<(), ssh2::Error> {
        let known_hosts_file = UserDirs::new()
            .map(|x| x.home_dir().join(".ssh/known_hosts"))
            .unwrap();
        std::process::Command::new("bash")
            .args(&[
                "-c",
                "sed -i -E '/(remote1|127.0.0.1)/d' ~/.ssh/known_hosts",
            ])
            .output()
            .unwrap();
        std::process::Command::new("bash")
            .args(&[
                "-c",
                "echo '[remote1]:32022 ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFJXgikelRn5cFqwK6SBSyvuXdMZsz8fIYi0ynE6kVlA' >> ~/.ssh/known_hosts"
            ])
            .output()
            .unwrap();
        check_known_hosts("127.0.0.1", 22, &known_hosts_file.to_string_lossy())?;
        check_known_hosts("remote1", 32022, &known_hosts_file.to_string_lossy())?;
        Ok(())
    }
}
